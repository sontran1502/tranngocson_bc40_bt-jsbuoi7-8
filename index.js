var arr = [];
document.querySelector("#txt-themSo").onclick = function (event) {
    event.preventDefault();
    var n = document.getElementById("txt-NhapN").value * 1;
    arr.push(n);
    document.getElementById("txt-NhapN").value = "";
    document.getElementById("txt-soArr").innerHTML = `👉 ${arr}`
}
// lession 1
function TinhTong() {
    var tong = 0;
    arr.forEach(function (item) {
        if (item >= 0) {
            tong += item;
        }
    })
    document.getElementById("result1").innerHTML = `👉 ${tong}`
}
// lession 2
document.querySelector("#txt-Dem").onclick = function () {
    var demSo = 0;
    arr.forEach(function (item) {
        if (item > 0) {
            demSo += 1;
        }
    })
    document.getElementById("result2").innerHTML = `👉 ${demSo}`
}
// lession 3
document.querySelector("#txt-soNhoNhat").onclick = function () {
    var min = arr[0];
    arr.forEach(function (item) {
        if (min > item) {
            min = item;
        }
    })
    document.getElementById("result3").innerHTML = `👉${min}`
}

// lession 4
document.getElementById("txt-soDNN").onclick = function () {
    var arrChan = [];
    arr.forEach(function (item) {
        if (item > 0) {
            arrChan.push(item);
        }
    })
    if (arrChan.length == 0) {
        document.getElementById("result4").innerHTML = `khônng có số dương`;

    } else {
        var min = arrChan[0];
        arrChan.forEach(function (item) {
            if (min > item) {
                min = item;
            }
        })
        document.getElementById("result4").innerHTML = `👉${min}`
    }
}

// lession 5

document.getElementById("txt-soChan").onclick = function () {
    var soChanCuoi = -1;
    for (var i = arr.length - 1; i >= 0; i--) {
        var soChan = arr[i];
        if (soChan % 2 == 0 && soChan != 0) {
            soChanCuoi = soChan;
            ; break
        }
    }
    document.getElementById("result5").innerHTML = `👉${soChanCuoi}`
}
// lession 6
document.getElementById("txt-swap").onclick = function () {
    var i1 = document.getElementById("txt-index1").value * 1;
    var i2 = document.getElementById("txt-index2").value * 1;
    var a = arr[i1];
    var b = arr[i2];
    arr.splice(i1, 1, b);
    arr.splice(i2, 1, a)
    document.getElementById("result6").innerHTML = `👉Mảng sau khi đổi: ${arr}`
}

// lession 7
function Arr_Sort(arr, n) {
    for (let i = 0; i < n - 1; ++i) {
        let min_index = i;
        for (let j = i + 1; j < n; ++j) {
            if (arr[j] < arr[min_index])
                min_index = j;
        }
        let temp = arr[i];
        arr[i] = arr[min_index];
        arr[min_index] = temp;
    }
}
document.getElementById("txt-SX").onclick = function () {
    Arr_Sort(arr, arr.length);
    document.getElementById("result7").innerHTML = `👉 Mảng sau khi sắp xếp: ${arr}`
}

// lession 8
function checkNT(n) {
    var checkSoNt = true;

    for (var i = 2; i <= Math.sqrt(n); i++) {
        if (n % i == 0) {
            checkNT = false;
        }
    }
    return checkSoNt
}
document.getElementById("txt-NguyenTo").onclick = function () {
    var soNT = false;
    var result8 = 0;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] >= 2) {
            soNT = checkNT(arr[i]);
            if (soNT == true) {
                result8 = arr[i];
                document.getElementById("result8").innerHTML = `👉  ${result8}`
                break;
            }
    }
    if(soNT==false){
        document.getElementById("result8").innerHTML = `👉 -1`
        
    }
}

// lession 9
var arrNew = [];
document.getElementById("txt-nhapSo").onclick = function () {
    var n1 = document.getElementById("txt-ThemSo_9").value * 1;
    arrNew.push(n1);
    document.getElementById("txt-ThemSo_9").value = "";
    document.getElementById("ThemSo").innerHTML = `👉  ${arrNew}`
}
document.getElementById("txt-demSoNguyen").onclick = function () {
    var dem = 0;
    arrNew.forEach(function (item) {
        if (Number.isInteger(item) == true) {
            dem += 1;
        }
    })
    document.getElementById("result9").innerHTML = `👉  ${dem}`
}

// lession 10
document.getElementById("txt-AD").onclick = function () {
    var nd = 0;
    var na = 0;
    var n = arr.length
    var result10 = "";
    arr.forEach(function (item) {
        if (item > 0) {
            nd += 1;
        }
        if (item < 0) {
            na += 1;
        }
    })
    if (nd > na) {
        result10 = " Số Dương > Số Âm"
    } else if (nd < na) {
        result10 = " Số Dương < Số Âm"
    } else {
        result10 = " Số Dương = Số Âm"
    }
    document.getElementById("result10").innerHTML = `👉  ${result10}`
}
}